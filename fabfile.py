from fabric.api import env, cd, run
from fabric.operations import put

path = '/var/www/jakecoffman.com'
env.host_string = '104.236.1.148'
env.user = 'root'  # TODO


def update():
    with cd(path):
        run('git pull origin master')
        put('secret.py', 'secret.py')
        run('pip install -r requirements.txt')
        run('python manage.py collectstatic --noinput')
        run('python manage.py migrate')
    restart()


def restart():
    run('/etc/init.d/apache2 restart')
