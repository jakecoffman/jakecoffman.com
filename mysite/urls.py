from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns(
    '',
    url(r'^$', 'mysite.views.index', name='index'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'login/', 'django.contrib.auth.views.login', name='login'),
    url(r'logout/', 'django.contrib.auth.views.logout', name='logout'),
)
